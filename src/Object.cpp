#include "../include/Object.h"

Object::Object() : m_isInteractive(false), m_isMovable(false), m_isMoved(false), m_isFounded(false) {

}

Object::~Object() = default;

bool Object::isMovable() const {
    return this->m_isMovable;
}

bool Object::isInteractive() const {
    return this->m_isInteractive;
}

bool Object::isFounded() const {
    return this->m_isFounded;
}

bool Object::isMoved() const {
    return this->m_isMoved;
}

void Object::setInteractive(const bool isInteractive) {
    this->m_isInteractive = isInteractive;
}

void Object::setMovable(const bool isMovable) {
    this->m_isMovable = isMovable;
}

void Object::interact() {

}

void Object::setCoordinates(const glm::vec4 &coordinates) {
    this->m_coordinates = coordinates;
}

glm::vec4 Object::getCoordinates() {
    return this->m_coordinates;
}

void Object::setModelMatrix(const glm::mat4 &matrix) {
    this->m_ModelMatrix = matrix;
}

glm::mat4 Object::getModelMatrix() {
    return this->m_ModelMatrix;
}