#include "../include/Scene.h"
#include "../include/Object.h"

Scene::Scene()
        : Shaders("shaders/3D.vs.glsl", "shaders/pointlight.fs.glsl"),
          Shaders_Room2("shaders/3D.vs.glsl", "shaders/tex3D.fs.glsl") {
    this->load_scene();
};

Scene::~Scene() {
};

void Scene::load_scene() {
    Model Shed((char *) "objets-3d/Room_1/Shed/CementShack2.obj");
    this->Models.push_back(Shed);

    Model Pallet((char *) "objets-3d/Room_1/Pallet/pallet.obj");
    this->Models.push_back(Pallet);

    Model Boxes((char *) "objets-3d/Room_1/Wooden_Boxes/wooden_boxes_pack.obj");
    this->Models.push_back(Boxes);

    Model Box_Confidential((char *) "objets-3d/Room_1/CardBoardBox_obj/CardBoardBox.obj");
    this->Models.push_back(Box_Confidential);

    Model Box((char *) "objets-3d/Room_1/Box/wooden_crate.obj");
    this->Models.push_back(Box);

    Model Corpse((char *) "objets-3d/Room_2/undead_sorcerer.obj");
    this->Models.push_back(Corpse);

    ObjectsManager::init();
};

void Scene::draw_scene(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    this->Shaders.use();
    draw_Shed(location, ProjMatrix, VMatrix);
    draw_Pallet(location, ProjMatrix, VMatrix);
    draw_Boxes(location, ProjMatrix, VMatrix);
    draw_Confidential(location, ProjMatrix, VMatrix);
    draw_Box(location, ProjMatrix, VMatrix);
};

void Scene::draw_Shed(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    glm::mat4 MVMatrix = VMatrix*ObjectsManager::objets[0]->getModelMatrix();
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
    this->Models[0].Draw(this->Shaders);
}

void Scene::draw_Pallet(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    glm::mat4 MVMatrix = VMatrix*ObjectsManager::objets[1]->getModelMatrix();;
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
    this->Models[1].Draw(this->Shaders);

    MVMatrix = VMatrix*ObjectsManager::objets[2]->getModelMatrix();;
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
    this->Models[1].Draw(this->Shaders);
}

void Scene::draw_Boxes(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    glm::mat4 MVMatrix = VMatrix*ObjectsManager::objets[3]->getModelMatrix();;
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));

    this->Models[2].Draw(this->Shaders);

    MVMatrix = VMatrix*ObjectsManager::objets[4]->getModelMatrix();;
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
    this->Models[2].Draw(this->Shaders);
}

void Scene::draw_Confidential(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    glm::mat4 MVMatrix = VMatrix;
    MVMatrix = glm::rotate(MVMatrix, glm::radians(90.f), glm::vec3(1.0f, 0.0f, 0.0));
    MVMatrix = glm::translate(MVMatrix, glm::vec3(2, -4.0, 2.1));
    MVMatrix = glm::scale(MVMatrix, glm::vec3(0.05, 0.05, 0.05));
    glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
    glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
    glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
    this->Models[3].Draw(this->Shaders);
}

void Scene::draw_Box(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    std::vector<glm::vec3> translate = {
            glm::vec3(1.2, -2.0, 3.8),
            glm::vec3(0.2, -2.0, 3.8),
            glm::vec3(-0.8, -2.0, 3.8),
            glm::vec3(1.2, -2.0, 2.8),
            glm::vec3(0.2, -2.0, 2.8),
            glm::vec3(-0.8, -2.0, 2.8),
            glm::vec3(-3.8, -2.0, 3.8),
            glm::vec3(-4.8, -2.0, 3.8),
            glm::vec3(-5.8, -2.0, 3.8),
            glm::vec3(-3.8, -2.0, 2.8)
    };

    for (int i = 0; i < 10; i++) {
        if(ObjectsManager::objets[6+i]->isMoved())
        {
            glm::mat4 MMatrix = glm::mat4();
            MMatrix = glm::translate(MMatrix, translate[i]);
            MMatrix = glm::scale(MMatrix, glm::vec3(0.5, 0.5, 0.5));
            ObjectsManager::objets[6+i]->setModelMatrix(MMatrix);
            ObjectsManager::objets[6+i]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));
        }

        glm::mat4 MVMatrix = VMatrix*ObjectsManager::objets[6+i]->getModelMatrix();;
        glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

        glUniformMatrix4fv(location["location_uMVMatrix"], 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(location["location_uMVPMatrix"], 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
        glUniformMatrix4fv(location["location_uNormalMatrix"], 1, GL_FALSE, glm::value_ptr(NormalMatrix));
        this->Models[4].Draw(this->Shaders);
    }
}

void Scene::draw_Room2(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix) {
    this->Shaders_Room2.use();
    draw_Shed(location,ProjMatrix,VMatrix);
};