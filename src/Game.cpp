#include "../include/Game.h"

Game::Game(const char *vertexShader, const char *fragmentShader, FilePath applicationPath)
        : CameraAngle(0.0), vertexShader(vertexShader), fragmentShader(fragmentShader), window(),
          applicationPath(applicationPath), program(), location(),
          Intersections(this->window.windowManager.width, this->window.windowManager.height), scene(), escape(false) {
    this->program = loadProgram(this->applicationPath.dirPath() + "shaders/" + this->vertexShader,
                                this->applicationPath.dirPath() + "shaders/" + this->fragmentShader);
    program.use();

    // Locations des variables uniform
    location["location_uMVPMatrix"] = glGetUniformLocation(program.getGLId(), "uMVPMatrix");
    location["location_uMVMatrix"] = glGetUniformLocation(program.getGLId(), "uMVMatrix");
    location["location_uNormalMatrix"] = glGetUniformLocation(program.getGLId(), "uNormalMatrix");
    location["location_uKd"] = glGetUniformLocation(program.getGLId(), "uKd");
    location["location_uKs"] = glGetUniformLocation(program.getGLId(), "uKs");
    location["location_uShininess"] = glGetUniformLocation(program.getGLId(), "uShininess");
    location["location_uLightIntensity"] = glGetUniformLocation(program.getGLId(), "uLightIntensity");

    location["location_uLightDir_vs"] = glGetUniformLocation(program.getGLId(), "uLightDir_vs");
    location["location_uLightPos_vs"] = glGetUniformLocation(program.getGLId(), "uLightPos_vs");

    // Active le test de profondeur
    glEnable(GL_DEPTH_TEST);
};

void Game::SetShaders(const char *vertexShader, const char *fragmentShader) {
    this->fragmentShader = fragmentShader;
    this->vertexShader = vertexShader;

    this->program = loadProgram(this->applicationPath.dirPath() + "shaders/" + this->vertexShader,
                                this->applicationPath.dirPath() + "shaders/" + this->fragmentShader);
    program.use();

    location["location_uLightDir_vs"] = glGetUniformLocation(program.getGLId(), "uLightDir_vs");
    location["location_uLightPos_vs"] = glGetUniformLocation(program.getGLId(), "uLightPos_vs");
    location["location_coeff"] = glGetUniformLocation(program.getGLId(), "coeff");
};

Game::~Game() {};

void Game::EventManagment(SDL_Event e) {
    if (e.type == SDL_KEYDOWN) {
        this->KeyBoardEvent(e);
    }

    if (e.type == SDL_MOUSEMOTION) {
        this->MouseMotionEvent(e);
    }

    if (e.type == SDL_MOUSEBUTTONUP) {
        this->MousePressEvent(e);
    }

}

void Game::KeyBoardEvent(SDL_Event e) {
    if (e.key.keysym.sym == SDLK_SPACE) {
        // Si object selectionnee envoie à grégoire
    }
    if (e.key.keysym.sym == SDLK_e) {
        if (glm::distance(glm::vec3(2, 0, -3.5), this->window.camera.getPosition()) <= 4) {
            ObjectsManager::objets[5]->interact();
        }
    }
    if (e.key.keysym.sym == SDLK_z) {
        this->window.camera.rotateLeft(-this->CameraAngle);
        this->CameraAngle = 0.0;
        this->window.camera.moveFront(0.1);
        if ((glm::distance(glm::vec3(4.5, 0.0, 4.2), this->window.camera.getPosition())) <= 0.5 &&
            ObjectsManager::objets[5]->isFounded() == true) {
            this->escape = true;
            this->window.camera.setViewMatrix();
            this->SetShaders("3D.vs.glsl", "tex3D.fs.glsl");
        }
    } else if (e.key.keysym.sym == SDLK_s) {
        this->window.camera.rotateLeft(-this->CameraAngle);
        this->CameraAngle = 0.0;
        this->window.camera.moveFront(-0.1);
    } else if (e.key.keysym.sym == SDLK_q) {
        this->window.camera.moveLeft(0.1);
    } else if (e.key.keysym.sym == SDLK_d) {
        this->window.camera.moveLeft(-0.1);
    }
};

void Game::MouseMotionEvent(SDL_Event e) {
    if (e.motion.state & SDL_BUTTON_LMASK) {
        if ((e.motion.yrel >= 0 && this->CameraAngle < 75.0) || (e.motion.yrel < 0 && this->CameraAngle > -75.0)) {
            this->window.camera.rotateLeft(e.motion.yrel / 10.0);
            this->CameraAngle += e.motion.yrel / 10.0;
        }
        this->window.camera.rotateUp(e.motion.xrel / 10.0);
    }
};

void Game::MousePressEvent(SDL_Event e) {
    if (e.button.button == SDL_BUTTON_RIGHT) {
        this->Intersections.setCameraCoordinate(this->window.camera.getPosition());
        int index = this->Intersections.TestRayIntersectionWithObj_2();
        if (index != -1) {
            ObjectsManager::objets[index]->interact();
        }
    }
};

void Game::IntroductionToGame() {
    ProjMatrix = glm::perspective(glm::radians(70.f), 800.0f / 600.0f, 0.1f, 100.f);

    // Definition des differents vecteurs et float
    uKd = glm::vec3(1.0, 1.0, 1.0);
    uKs = glm::vec3(1.0, 1.0, 1.0);
    uLightPos_vs = glm::vec4(1.0, 1.0, 1.0, 0.0);
    uLightDir_vs = glm::vec4(1.0, 1.0, 1.0, 0.0);
    uLightIntensity = glm::vec3(2.0, 2.0, 2.0);
    uShininess = 10;

}

void Game::FirstRoom() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    MVMatrix = this->window.camera.getViewMatrix();
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    // Couleur et lumiere
    uLightPos_vs = this->window.camera.getViewMatrix() * glm::vec4(1.0, 1.0, 1.0, 0.0);
    glUniform3f(location["location_uLightDir_vs"], uLightPos_vs.x, uLightPos_vs.y, uLightPos_vs.z);

    glUniform3fv(location["location_uLightIntensity"], 1, glm::value_ptr(uLightIntensity));
    glUniform3fv(location["location_uKd"], 1, glm::value_ptr(uKd));
    glUniform3fv(location["location_uKs"], 1, glm::value_ptr(uKs));
    glUniform1f(location["location_uShininess"], uShininess);

    scene.draw_scene(location, ProjMatrix, MVMatrix);

    glBindVertexArray(0);

    // Update the display
    this->window.windowManager.swapBuffers();
}

void Game::SecondRoom() {


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    MVMatrix = this->window.camera.getViewMatrix();
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    uLightDir_vs = this->window.camera.getViewMatrix() * glm::vec4(1.0, 1.0, 1.0, 0.0);
    glUniform3f(location["location_uLightDir_vs"], uLightDir_vs.x, uLightDir_vs.y, uLightDir_vs.z);


    glUniform3fv(location["location_uLightIntensity"], 1, glm::value_ptr(uLightIntensity));
    glUniform3fv(location["location_uKd"], 1, glm::value_ptr(uKd));
    glUniform3fv(location["location_uKs"], 1, glm::value_ptr(uKs));
    glUniform1f(location["location_uShininess"], uShininess);

    scene.draw_Room2(location, ProjMatrix, MVMatrix);

    glBindVertexArray(0);

    // Update the display
    this->window.windowManager.swapBuffers();
}

