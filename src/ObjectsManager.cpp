#include "../include/ObjectsManager.h"

std::vector<Object*> ObjectsManager::objets;

void ObjectsManager::init() {

    objets.reserve(15);

    //Batiment 1, coordone et ModelMatrix
    objets.push_back(new Object());
    // ModelMatrix
    glm::mat4 MMatrix = glm::mat4();
    MMatrix = glm::translate(MMatrix, glm::vec3(0.0, -3.0, 0.0));
    MMatrix = glm::scale(MMatrix, glm::vec3(1, 1, 1));
    objets[0]->setModelMatrix(MMatrix);
    // Coordinates
    objets[0]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));



    //Palettes 2,3
    objets.push_back(new Object());
    MMatrix = glm::mat4();
    MMatrix = glm::rotate(MMatrix, glm::radians(90.f), glm::vec3(0.0, 1.0f, 0.0));
    MMatrix = glm::translate(MMatrix, glm::vec3(-3.0, -2.7, -4.9));
    MMatrix = glm::scale(MMatrix, glm::vec3(0.80, 0.8, 0.8));
    objets[1]->setModelMatrix(MMatrix);
    // Coordinates
    objets[1]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));

    objets.push_back(new Object());
    MMatrix = glm::mat4();
    MMatrix = glm::rotate(MMatrix, glm::radians(90.f), glm::vec3(0.0, 1.0f, 0.0));
    MMatrix = glm::translate(MMatrix, glm::vec3(-3.0, -2.7, 0));
    MMatrix = glm::scale(MMatrix, glm::vec3(0.8, 0.8, 0.8));
    objets[2]->setModelMatrix(MMatrix);
    // Coordinates
    objets[2]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));

    //Tas de box 4 et 5
    objets.push_back(new Object());
    MMatrix = glm::mat4();
    MMatrix = glm::rotate(MMatrix, glm::radians(180.f), glm::vec3(0.0, 1.0f, 0.0));
    MMatrix = glm::translate(MMatrix, glm::vec3(-4.6, -3, 1.8));
    MMatrix = glm::scale(MMatrix, glm::vec3(0.5, 0.5, 0.5));
    objets[3]->setModelMatrix(MMatrix);
    // Coordinates
    objets[3]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));

    objets.push_back(new Object());
    MMatrix = glm::mat4();
    MMatrix = glm::rotate(MMatrix, glm::radians(270.f), glm::vec3(0.0, 1.0f, 0.0));
    MMatrix = glm::translate(MMatrix, glm::vec3(-2.0, -3, 4.1));
    MMatrix = glm::scale(MMatrix, glm::vec3(0.5, 0.5, 0.5));
    objets[4]->setModelMatrix(MMatrix);
    // Coordinates
    objets[4]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));

    //Boite confidentielle 6
    objets.push_back(new File());
    MMatrix = glm::rotate(MMatrix, glm::radians(90.f), glm::vec3(1.0f, 0.0f, 0.0));
    MMatrix = glm::translate(MMatrix, glm::vec3(2, -4.0, 2.1));
    MMatrix = glm::scale(MMatrix, glm::vec3(0.05, 0.05, 0.05));
    objets[5]->setModelMatrix(MMatrix);
    // Coordinates
    objets[5]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));

    std::vector<glm::vec3> translate = {
            glm::vec3(3, -2.45, -2.7),
            glm::vec3(2, -2.45, -3),
            glm::vec3(1, -2.45, -2.5),
            glm::vec3(1, -2.45, -3.8),
            glm::vec3(1, -1.5, -3.5),
            glm::vec3(2, -2.45, -2),
            glm::vec3(1.7, -1.45, -2.5),
            glm::vec3(2.7, -1.5, -2.8),
            glm::vec3(1.4, -0.5, -3.8),
            glm::vec3(2, -0.5, -2.8)
    };

    //Boites simples 7 à 16
    for (int i = 0; i < 10; ++i) {
        objets.push_back(new Box());
        glm::mat4 MMatrix = glm::mat4();
        MMatrix = glm::translate(MMatrix, translate[i]);
        MMatrix = glm::scale(MMatrix, glm::vec3(0.5, 0.5, 0.5));
        objets[6+i]->setModelMatrix(MMatrix);
        // Coordinates
        objets[6+i]->setCoordinates(MMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0));
    }
}
