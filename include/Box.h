#include "Object.h"

#ifndef MAIN_CPP_BOX_H
#define MAIN_CPP_BOX_H


class Box : public Object {

public:
    Box();

    ~Box();

    void interact() override;
    inline void setIsMovable(bool boolean){
        this->m_isMovable = false;
    }
};


#endif //MAIN_CPP_BOX_H
