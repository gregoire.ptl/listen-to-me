#ifndef JEU_GAME_H
#define JEU_GAME_H

#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include "Windows.h"
#include <map>
#include "RayIntersection.h"
#include "Scene.h"

using namespace glimac;

class Game {
public:
    Windows window;
    bool escape;

    RayIntersection Intersections;

    Game(const char* vertexShader, const char* fragmentShader, FilePath applicationPath);
    ~Game();

    void IntroductionToGame();
    void FirstRoom();
    void SecondRoom();

    void EventManagment(SDL_Event e);

    void SetShaders(const char* vertexShader, const char* fragmentShader);

private:
    FilePath applicationPath;
    const char* vertexShader;
    const char* fragmentShader;

    float CameraAngle;
    float coordX;
    float coordY;

    Program program;
    std::map<std::string, GLint> location;

    void KeyBoardEvent(SDL_Event e);
    void MouseMotionEvent(SDL_Event e);
    void MousePressEvent(SDL_Event e);

    glm::mat4 ProjMatrix;
    glm::mat4 MVMatrix;
    glm::mat4 NormalMatrix;

    glm::vec3 uKd;
    glm::vec3 uKs;
    glm::vec3 uLightIntensity;

    glm::vec4 uLightDir_vs;
    glm::vec4 uLightPos_vs;

    float uShininess;

    Scene scene;
};
#endif //JEU_GAME_H
