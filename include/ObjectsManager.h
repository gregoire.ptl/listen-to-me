
#ifndef MAIN_CPP_OBJECTSMANAGER_H
#define MAIN_CPP_OBJECTSMANAGER_H

#include <vector>
#include "Object.h"
#include "File.h"
#include "Box.h"


class ObjectsManager {

public:
    static std::vector<Object*> objets;

    static void init();

};


#endif //MAIN_CPP_OBJECTSMANAGER_H
