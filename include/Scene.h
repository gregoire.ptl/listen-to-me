#ifndef JEU_SCENE_H
#define JEU_SCENE_H

#include "Model.h"
#include "Shaders_Assimp.h"
#include <glimac/FreeflyCamera.hpp>
#include <map>
#include "ObjectsManager.h"
#include "Object.h"


class Scene{
public:
    Scene();
    ~Scene();

    void load_scene();
    void draw_scene(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
    void draw_Room2(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);

private:
    std::vector<Model> Models;
    Shaders_Assimp Shaders;
    Shaders_Assimp Shaders_Room2;
    void draw_Shed(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
    void draw_Pallet(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
    void draw_Boxes(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
    void draw_Confidential(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
    void draw_Box(std::map<std::string, GLint> location, glm::mat4 ProjMatrix, glm::mat4 VMatrix);
};


#endif //JEU_SCENE_H
