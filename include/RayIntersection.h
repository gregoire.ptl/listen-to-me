#ifndef JEU_RAYINTERSECTION_H
#define JEU_RAYINTERSECTION_H

#include <glm/glm.hpp>
#include <vector>
#include <math.h>
#include "ObjectsManager.h"


class RayIntersection {
public:
    RayIntersection(float screenWidth, float screenHeight);
    ~RayIntersection();


    inline void setMouseXY(float newX, float newY){
        this->mouseX = newX;
        this->mouseY = newY;
    };

    inline void setModelMatrix(glm::mat4 newModelMatrix){
        this->ModelMatrix = newModelMatrix;
    };

    inline void setProjectionMatrix(glm::mat4 newProjectionMatrix){
        this->ProjectionMatrix = newProjectionMatrix;
    };

    inline void setViewMatrix(glm::mat4 newViewMatrix){
        this->ViewMatrix = newViewMatrix;
    };

    inline void setCameraCoordinate(glm::vec3 newCameraCoordinate){
        this->cameraCoordinate = newCameraCoordinate;
    };

    void setRayDirectionAndOrigin();
    int TestRayIntersectionWithObj();
    int TestRayIntersectionWithObj_2();
    bool TestRayIntersectionXaxis(int indice);
    bool TestRayIntersectionYaxis(int indice);

private:
    float mouseX;
    float mouseY;
    float screenWidth;
    float screenHeight;

    glm::mat4 ProjectionMatrix;
    glm::mat4 ModelMatrix;
    glm::mat4 ViewMatrix;

    glm::vec3 rayDirection;
    glm::vec3 rayOrigin;
    glm::vec3 cameraCoordinate;
};


#endif //JEU_RAYINTERSECTION_H
