#ifndef JEU_MODEL_H
#define JEU_MODEL_H

#include "mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <stb/stb_image.h>
#include "Shaders_Assimp.h"

class Model {
public:
    Model(char *path){
        loadModel(path);
    }
    void Draw(Shaders_Assimp &shader);

private:
    std::vector<Texture> textures_loaded;
    std::vector<mesh> Meshes;
    std::string directory;

    void loadModel(std::string path);
    void processNode(aiNode *node, const aiScene *scene);
    mesh processMesh(aiMesh *Mesh, const aiScene *scene);
    std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
    unsigned int TextureFromFile(const char *path, const std::string &directory);
};
#endif //JEU_MODEL_H
