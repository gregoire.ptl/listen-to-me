//
// Created by Gregoire on 30/12/2020.
//
//mix playing
#ifndef JEU_SOUNDMANAGER_H
#define JEU_SOUNDMANAGER_H

#include "SDL_mixer.h"

#include <assert.h>
#include <dirent.h>
#include <vector>
#include <string>
#include <iostream>


struct Sound
{
    char _soundName [300];
    Mix_Chunk * _soundNoise;
};


class SoundManager {

public:

    SoundManager();

    static void init(const char * room); //a lancer au debut pour chargela "room"

    static void close(); //a la fin pour tout fermer et vider la mémoire

    void static setMusicAmbiance(const char *filePath, const int volume = 128); //pour charger et lancer la musique d'ambiance

    static void playSound( const char *soundToPlayName); // pour jouer le sond dont le nom de fichier est soundToPlayName

    ~SoundManager();

private:

    static const int SOUND_SAMPLING = 44100;
    static int CHANNELS;

    static std::vector<Sound*> s_tabSound; //tableau contenant toutes les structures sond
    static Mix_Music *s_ambiance; // pointeur pour la musique d'ambiance

    static void loadSounds(const char *filePath); // charge tous les son du dossier filePath /!\ le dossier doit contenir que des musiques
    static void loadOneSound(dirent *& file, const char *fullPath);
    static Sound * getSound(const char *soundToPlayName); // pour avoir le son en le cherchant par son nom
    static int channelToPlay(const Mix_Chunk *sound); // pour savoir sur quel channel jouer la musique (si tout est pris joue sur CHANNEL-1)
    static bool isPlaying (const Mix_Chunk *sound); //es ce que le son est déja en train d'être jouer
    static int findFreeChannel(); // trouver un channel libre
};


#endif //JEU_SOUNDMANAGER_H
