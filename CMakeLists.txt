cmake_minimum_required(VERSION 3.13)

project(jeu)
add_definitions(-std=c++17)
set(CMAKE_CXX_FLAGS_INIT "-W -Wall -Werror")
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

find_package(SDL REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(ASSIMP REQUIRED)

include_directories(${SDL_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR} ${GLEW_INCLUDE_DIR} ${ASSIMP_INCLUDE_DIR} include glimac/include)
set(ALL_LIBRARIES glimac ${SDL_LIBRARY} ${OPENGL_LIBRARIES} ${GLEW_LIBRARY} ${ASSIMP_LIBRARIES})
add_subdirectory(glimac)

file(GLOB_RECURSE HEADER_FILES include/*.h)
file(GLOB_RECURSE SRC_FILES src/*.cpp)
file(GLOB_RECURSE SHADER_FILES *.glsl)

add_executable(jeu main.cpp ${SRC_FILES} ${HEADER_FILES} ${SHADER_FILES})
target_link_libraries(jeu ${ALL_LIBRARIES} -lSDL_mixer)

file(COPY shaders DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY objets-3d DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY sounds DESTINATION ${CMAKE_CURRENT_BINARY_DIR})