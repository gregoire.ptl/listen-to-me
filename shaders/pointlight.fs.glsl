#version 300 es

precision mediump float;

// Entrée du shader
in vec3 vPosition_vs; // Position du sommet transformé dans l'espcace View
in vec3 vNormal_vs; // Normale du sommet tranformé dans l'espace View
in vec2 vTexCoords; // Coordonnées de texture du sommets

out vec3 fFragColor;

// Information sur le matériaux de l'objet
uniform vec3 uKd;
uniform vec3 uKs;
uniform float uShininess;

// Information sur la lumière
uniform vec3 uLightPos_vs;
uniform vec3 uLightIntensity;

uniform sampler2D texture_diffuse1;

vec3 blinnPhong(){
  vec3 wi = normalize(uLightPos_vs - vPosition_vs);
  vec3 wo = normalize(-vPosition_vs);
  vec3 halfVector = normalize((wi + wo)/2.0);
  float dist = distance(uLightPos_vs,vPosition_vs);
  return(1.2*(vec3(texture(texture_diffuse1, vTexCoords).rgb))/(dist)*(normalize(uKd)*(dot(wi,normalize(vNormal_vs)))+normalize(uKs)*pow(dot(halfVector,normalize(vNormal_vs)),uShininess)));
}

void main(){
  fFragColor = blinnPhong();
}
