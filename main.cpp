#include <glimac/Program.hpp>
#include "include/Game.h"
#include "include/File.h"
#include "include/SoundManager.h"

int main(int argc, char **argv) {

    Game newGame("3D.vs.glsl", "pointlight.fs.glsl", argv[0]);
    newGame.IntroductionToGame();

    SoundManager::init("room1");

    // Application loop:
    bool done = false;
    while (!done) {
        // Event loop:
        SDL_Event e;
        while (newGame.window.windowManager.pollEvent(e)) {
            if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)) {
                done = true; // Leave the loop after this iteration
            }
            newGame.EventManagment(e);
        }
        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/

        if (!newGame.escape) {
            newGame.FirstRoom();
        } else {
            newGame.SecondRoom();
        }
    }
    SoundManager::close();
    SDL_Quit();
    return EXIT_SUCCESS;
}
