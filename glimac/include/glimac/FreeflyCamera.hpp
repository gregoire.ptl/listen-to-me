#ifndef FREEFLYCAMERA_HPP__
#define FREEFLYCAMERA_HPP__


class FreeflyCamera{
private:
  glm::vec3 m_Position; // Position de la camera
  float m_fPhi;
  float m_fTheta;
  glm::vec3 m_FrontVector; // Vecteur F
  glm::vec3 m_LeftVector; // Vecteur L
  glm::vec3 m_UpVector; // Vecteur U

public:
  // Constructeur
  ~FreeflyCamera(){};
  FreeflyCamera();

  // Methode
  void computeDirectionVectors();
  void moveLeft(float t);
  void moveFront(float t);
  void rotateLeft(float degrees);
  void rotateUp(float degrees);
  glm::mat4 getViewMatrix() const;

  inline glm::vec3 getPosition(){
      return m_Position;
  }

  inline void setViewMatrix(){
      this->m_Position = glm::vec3(0.0,0.0,0.0);
      this->m_fPhi = glm::pi<float>();
      this->m_fTheta = 0.0;
      computeDirectionVectors();
  }
};

#endif
