#ifndef TRACKBALLCAMERA_HPP__
#define TRACKBALLCAMERA_HPP__


class TrackballCamera{
private:
  float m_fDistance;
  float m_fAngleX;
  float m_fAngleY;

public:
  // Constructeur
  ~TrackballCamera(){};
  TrackballCamera();

  // Methode
  void moveFront(float delta);
  void rotateLeft(float degrees);
  void rotateUp(float degrees);

  glm::mat4 getViewMatrix() const;
};

#endif
